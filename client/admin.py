from django.contrib import admin
from django.forms.models import ModelForm
from django.forms.fields import CharField, TextInput
from modeltranslation.admin import TranslationAdmin
from django.utils.translation import ugettext_lazy as _

from client.models import Client


@admin.register(Client)
class ClientAdmin(TranslationAdmin):
    raw_id_fields = ('user',)
    list_display = ('user', 'phone_number')
