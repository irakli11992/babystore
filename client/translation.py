from modeltranslation.translator import register, TranslationOptions
from client.models import Client


@register(Client)
class ClientTranslationOptions(TranslationOptions):
    pass
