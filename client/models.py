from django.contrib.auth.models import User
from django.db import models
from django_phonenumbers.model.fields import PhoneNumberField
from django.utils.translation import ugettext_lazy as _


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_("User"))
    phone_number = PhoneNumberField(verbose_name=_("Phone number"))
    shipping_address = models.CharField(max_length=500, verbose_name=_("Shipping address"),
                                        help_text=_('Maximum %(count)d symbols are allowed') % {"count": 500})
    billing_address = models.CharField(max_length=500, verbose_name=_("Billing address"),
                                       help_text=_('Maximum %(count)d symbols are allowed') % {"count": 500})

    def __str__(self):
        return str(_('Client'))

    class Meta:
        verbose_name = _('Client')
        verbose_name_plural = _('Clients')
