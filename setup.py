from distutils.core import setup

setup(
    name='babystore',
    version='1.0.1',
    packages=['cart', 'cart.migrations', 'items', 'items.migrations', 'stock', 'stock.migrations', 'client',
              'client.migrations', 'orders', 'orders.migrations', 'babystore'],
    url='',
    license='',
    author='irakli khitarishvili',
    author_email='irakli11992@gmail.com',
    description=''
)
