from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from reactfilebrowser.views import get_root, get_browser, test

urlpatterns = [
    url(r'root/$', get_root, name="root"),
    url(r'browser/(?P<opener_id>[-\w]+)/$', get_browser, name="browser"),
    url(r'browser/$', get_browser, name="browser"),
    url(r'test/$', test, name="test"),
]
urlpatterns = format_suffix_patterns(urlpatterns)
