from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django.forms.widgets import TextInput
from django.conf import settings


class FileBrowserWidget(TextInput):
    def render(self, name, value, attrs=None):
        input_tag = super().render(name, value, attrs)
        button = mark_safe(
            """
            <button
                    data-rfb-target="{target}"
                    data-rfn-root-api="{root_api}"
                    class="react-file-browser-field">
                    open chooser
            </button>
            """.format(
                target="{id}".format(id=attrs.get("id", "undefined")),
                root_api=reverse("reactfilebrowser:browser"),
            )
        )
        return mark_safe(button + input_tag)

    class Media:
        js = (
            settings.STATIC_URL + '/reactfilebrowser/bundle.js',
            settings.STATIC_URL + '/reactfilebrowser/file_browser_field.js'
        )
