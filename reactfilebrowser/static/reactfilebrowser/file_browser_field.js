$(document).ready(
    function () {
        $(".react-file-browser-field").each(
            function () {
                var $self = $(this);
                $self.on("click", function (e) {
                    e.preventDefault();
                    var params = [
                        'height=' + screen.height,
                        'width=' + screen.width,
                        'fullscreen=yes' // only works in IE, but here for completeness
                    ].join(',');
                    window.open($self.attr("data-rfn-root-api") + $self.attr("data-rfb-target"), "File Browser", params);
                    window.onmessage = function (e) {
                        document.getElementById($self.attr("data-rfb-target")).value = JSON.parse(e.data).join(',');
                    }
                })
            }
        )
    }
);
