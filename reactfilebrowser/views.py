from django.conf import settings
from django.http.response import JsonResponse
from django.shortcuts import render

from reactfilebrowser.helpers import path_to_dict


def get_root(request):
    path_dict = path_to_dict(settings.MEDIA_ROOT)
    resp = JsonResponse(path_dict)
    resp["Access-Control-Allow-Origin"] = "*"
    return resp


def get_browser(request, opener_id=None):
    response = render(request, "reactfilebrowser/index.html", {"opener_id": opener_id, "settings": settings})
    response["Access-Control-Allow-Origin"] = "*"
    return response


def test(request):
    return render(request, "reactfilebrowser/test.html")
