import os
import json
from uuid import uuid4

from django.conf import settings


def path_to_dict(path):
    d = {'name': os.path.basename(path), 'id': str(uuid4())}
    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = [path_to_dict(os.path.join(path, x)) for x in os.listdir(path)]
    else:
        d['type'] = "file"
        d["extension"] = os.path.basename(path).split('.')[-1].lower()
        d["data"] = "{0},{1}".format(settings.MEDIA_URL, "data")
    return d


def get_json_path(path):
    return json.dumps(path_to_dict(path))
