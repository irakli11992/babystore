from django.conf import settings


def get_settings(request):
    return {
        "MEDIA_URL": settings.MEDIA_URL
    }
