from django import template

register = template.Library()


@register.inclusion_tag("utils/header.html")
def header_tag():
    return {}


@register.inclusion_tag("items/product_list/header_footer/paging.html")
def paging_tag(paginator, page_obj):
    return locals()
