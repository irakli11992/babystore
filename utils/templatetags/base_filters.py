from django import template
from django.utils.translation import get_language

register = template.Library()


@register.filter()
def translate(value, field):
    return getattr(value, "{0}_{1}".format(field, get_language()))
