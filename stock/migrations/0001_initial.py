# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-05 07:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('items', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='მაქსიმუმ 150 სიმბოლოა დასაშვები', max_length=150, verbose_name='Name')),
                ('name_ka', models.CharField(help_text='მაქსიმუმ 150 სიმბოლოა დასაშვები', max_length=150, null=True, verbose_name='Name')),
                ('name_en', models.CharField(help_text='მაქსიმუმ 150 სიმბოლოა დასაშვები', max_length=150, null=True, verbose_name='Name')),
                ('address', models.CharField(help_text='მაქსიმუმ 200 სიმბოლოა დასაშვები', max_length=200, verbose_name='Address')),
                ('address_ka', models.CharField(help_text='მაქსიმუმ 200 სიმბოლოა დასაშვები', max_length=200, null=True, verbose_name='Address')),
                ('address_en', models.CharField(help_text='მაქსიმუმ 200 სიმბოლოა დასაშვები', max_length=200, null=True, verbose_name='Address')),
            ],
            options={
                'verbose_name': 'Stock',
                'verbose_name_plural': 'Stocks',
            },
        ),
        migrations.CreateModel(
            name='StockItemMembership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=0, verbose_name='Quantity')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.Item', verbose_name='Items')),
                ('stock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stock.Stock', verbose_name='Stock')),
            ],
            options={
                'verbose_name': 'Item by stock',
                'verbose_name_plural': 'Items by stock',
            },
        ),
        migrations.AddField(
            model_name='stock',
            name='items',
            field=models.ManyToManyField(through='stock.StockItemMembership', to='items.Item', verbose_name='Item'),
        ),
        migrations.AlterUniqueTogether(
            name='stockitemmembership',
            unique_together=set([('item', 'stock')]),
        ),
    ]
