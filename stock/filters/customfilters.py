from django.contrib import admin
from django.utils.translation import ugettext_lazy as _


class LookUpFilter(admin.SimpleListFilter):
    def queryset(self, request, queryset):
        pass

    def lookups(self, request, model_admin):
        pass

    template = "custom_template.html"


class QuantityFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Quantity')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'quantity'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('50', 'X<50'),
            ('100', '50<x<100'),
            ('1000', '100<x<1000'),
            ('all', '1000<x'),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == '50':
            return queryset.filter(quantity__gte=0,
                                   quantity__lte=50)
        if self.value() == '100':
            return queryset.filter(quantity__gte=50,
                                   quantity__lte=100)
        if self.value() == '1000':
            return queryset.filter(quantity__gte=100,
                                   quantity__lte=1000)
        if self.value() == 'all':
            return queryset.filter(quantity__gte=1000)
