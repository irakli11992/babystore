from modeltranslation.translator import register, TranslationOptions
from stock.models import Stock, StockItemMembership


@register(Stock)
class StockTranslationOptions(TranslationOptions):
    fields = ('name', 'address')


@register(StockItemMembership)
class StockItemMembershipTranslationOptions(TranslationOptions):
    pass
