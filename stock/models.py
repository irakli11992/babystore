from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language

from items.models import Item


class Stock(models.Model):
    name = models.CharField(max_length=150, verbose_name=_('Name'),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 150})
    address = models.CharField(max_length=200, verbose_name=_('Address'),
                               help_text=_('Maximum %(count)d symbols are allowed') % {"count": 200})
    items = models.ManyToManyField(Item, verbose_name=_('Item'), through='StockItemMembership')

    def __str__(self):
        return self.name_ka if get_language() == "ka" else self.name_en

    class Meta:
        verbose_name = _('Stock')
        verbose_name_plural = _('Stocks')


class StockItemMembership(models.Model):
    item = models.ForeignKey(Item, verbose_name=_('Items'))
    stock = models.ForeignKey(Stock, verbose_name=_('Stock'))
    quantity = models.IntegerField(verbose_name=_("Quantity"), default=0)

    def get_item_size(self):
        return "{0}-{1}".format(self.item.age.age_from, self.item.age.age_to)

    get_item_size.short_description = _('Age range')

    def get_item_brand(self):
        if self.item.brand is None:
            return '-'
        return self.item.brand.name_ka if get_language() == "ka" else self.item.brand.name_en

    get_item_brand.short_description = _('Brand')

    def __str__(self):
        return str(_('Item'))

    class Meta:
        unique_together = ('item', 'stock')
        verbose_name = _('Item by stock')
        verbose_name_plural = _('Items by stock')
