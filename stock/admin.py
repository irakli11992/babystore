from django.contrib import admin
from django.contrib.admin.filters import RelatedOnlyFieldListFilter
from modeltranslation.admin import TranslationAdmin

from stock.filters.customfilters import QuantityFilter
from stock.models import Stock, StockItemMembership


# class InlineItem(TranslationTabularInline):
#     model = Stock.item.through
#     raw_id_fields = ('item',)
#     extra = 1


@admin.register(Stock)
class StockAdmin(TranslationAdmin):
    fields = ('name', 'address',)
    # filter_horizontal = ('item',)


@admin.register(StockItemMembership)
class StockItemMembershipAdmin(admin.ModelAdmin):
    list_display = ('stock', 'item', 'get_item_brand', 'get_item_size', 'quantity')
    raw_id_fields = ('item', 'stock')
    list_filter = ('item__age', 'item__brand', ('stock', RelatedOnlyFieldListFilter), QuantityFilter)
    list_per_page = 20

    class Media:
        js = ['js/collapsible_filters.js']
