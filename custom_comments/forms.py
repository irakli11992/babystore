from django import forms
from django.utils.translation import ugettext_lazy as _
from django_comments.forms import CommentForm


class CommentFormWithTitle(CommentForm):
    RATING = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    title = forms.CharField(
        max_length=300,
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control', 'placeholder': _("Title")
        }))
    comment = forms.CharField(
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'placeholder': _("Comment"),
            "style": "resize:none"}
        ))
    rating = forms.ChoiceField(choices=RATING, widget=forms.Select(attrs={
        'class': 'btn btn-default ',
    }))
    name = forms.CharField(
        max_length=300,
        widget=forms.TextInput(attrs={
            'class': 'form-control', 'placeholder': _("Name")
        }))
    email = forms.CharField(
        max_length=300,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': _("Email Address"),
            "type": "email"
        }))
    honeypot = forms.CharField(required=False, widget=forms.HiddenInput,
                               label=_('If you enter anything in this field '
                                       'your comment will be treated as spam'))
    url = forms.URLField(label=_("URL"), required=False, widget=forms.HiddenInput)

    def get_comment_create_data(self):
        # Use the data of the superclass, and add in the title field
        data = super(CommentFormWithTitle, self).get_comment_create_data()
        data['title'] = self.cleaned_data['title']
        data['rating'] = self.cleaned_data['rating']
        return data
