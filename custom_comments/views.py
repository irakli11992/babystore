from django.shortcuts import render
from django.views.generic import View

from items.models import Item


class CommentList(View):
    template_name = 'custom_comments/index.html'

    def get(self, request, object_slug, page, *args, **kwargs):
        entry = Item.objects.get(slug=object_slug)
        return render(request, self.template_name, locals())

