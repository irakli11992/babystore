from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from custom_comments.views import CommentList

urlpatterns = [
    url(r'list/(?P<object_slug>[a-z0-9-]+)/(?P<page>[0-9]+)/$', CommentList.as_view(), name="custom_comments_list"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
