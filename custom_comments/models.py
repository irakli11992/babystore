from django.contrib.auth.models import User
from django.db import models
from django_comments.models import CommentAbstractModel, Comment


class CommentWithTitle(CommentAbstractModel):
    title = models.CharField(max_length=300)
    rating = models.FloatField(null=True, blank=True)

    class Meta:
        unique_together = ("object_pk", 'user')

        # query=(c.CommentWithTitle.select(fn.Max(c.CommentWithTitle.id).alias('id')).group_by(c.CommentWithTitle.rating))
        # c.CommentWithTitle.select().join(query.alias("jq"),on=(c.CommentWithTitle.id == query.alias('jq').c.id).alias('log')).sql()
