def get_model():
    from custom_comments.models import CommentWithTitle
    return CommentWithTitle


def get_form():
    from custom_comments.forms import CommentFormWithTitle
    return CommentFormWithTitle
