from string import Template

from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

register = template.Library()


def get_star_list(rating):
    rating = 0 if rating is None else rating
    star_template = Template('<span class="star $status"></span>')
    stars = [
        star_template.safe_substitute(
            status="active" if star <= rating else ""
        ) for star in range(1, 6)
        ]
    return stars


def get_rating(rating):
    rating = 0 if rating is None else rating
    star_template = Template('<span class="star $status"></span>')
    stars = [
        star_template.safe_substitute(
            status="active"
            if star <= rating
            else "" if type(star - rating) is int
            else "" if star - rating >= 0.8
            else "active" if star - rating <= 0.2 else "half"  # checking halves
        ) for star in range(1, 6)
        ]
    return stars


@register.filter(needs_autoescape=True)
def get_stars(rating, autoescape=True):
    return mark_safe(
        Template('<div class="rating">$stars</div>').safe_substitute(stars=' '.join(get_star_list(rating))))


@register.filter
def get_avg_star(value):
    try:
        if type(value) is float:
            avg = value
        else:
            avg = value._meta.model.item_manager.get_avg_rating(value.id)[0]
    except:
        avg = 0
    return mark_safe(' '.join(get_rating(avg)))
