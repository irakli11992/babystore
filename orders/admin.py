from django.contrib import admin
from django.contrib.admin.options import TabularInline
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from orders.models import Order, OrderLine, OrderStatus, OrderType


class OrderLineInline(TabularInline):
    model = OrderLine
    raw_id_fields = ('product',)


@admin.register(Order)
class OrderAdmin(TranslationAdmin):
    inlines = (OrderLineInline,)


@admin.register(OrderStatus)
class OrderStatusAdmin(TranslationAdmin):
    list_display = ('name', 'description')


@admin.register(OrderType)
class OrderTypeAdmin(TranslationAdmin):
    list_display = ('name', 'description')
