from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from products.models import Product


class Order(models.Model):
    buyer = models.ForeignKey(User, verbose_name=_("Buyer"))
    unique_code = models.UUIDField(default=uuid4, verbose_name=_("Unique_code"))
    description = models.TextField(verbose_name=_("Description"))
    type = models.ForeignKey('OrderType', verbose_name=_("Type"))
    status = models.ForeignKey('OrderStatus', verbose_name=_("Status"))
    order_placed = models.DateTimeField(verbose_name=_("Order placed"))
    order_finished = models.DateTimeField(verbose_name=_("Order finished"))

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")
        permissions = (
            ("view_orders", "Can see available orders"),
            # ("change_task_status", "Can change the status of tasks"),
            # ("close_task", "Can remove a task by setting its status as closed"),
        )


class OrderStatus(models.Model):
    name = models.CharField(max_length=150, verbose_name=_('Name'),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 150})
    description = models.TextField(verbose_name=_('Description'))

    class Meta:
        verbose_name = _("Status")
        verbose_name_plural = _("Statuses")


class OrderType(models.Model):
    name = models.CharField(max_length=150, verbose_name=_('Name'),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 150})
    description = models.TextField(verbose_name=_('Description'))

    class Meta:
        verbose_name = _("Type")
        verbose_name_plural = _("Types")


class OrderLine(models.Model):
    order = models.ForeignKey('Order', verbose_name=_("Order"))
    product = models.ForeignKey(Product, verbose_name=_("Product"))
    quantity = models.IntegerField(verbose_name=_("Quantity"))
    amount = models.DecimalField(max_digits=11, decimal_places=4, verbose_name=_("Amount"))

    class Meta:
        permissions = (
            ("view_order_line", "Can see available order lines"),
            # ("change_task_status", "Can change the status of tasks"),
            # ("close_task", "Can remove a task by setting its status as closed"),
        )
