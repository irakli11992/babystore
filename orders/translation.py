from modeltranslation.translator import register, TranslationOptions

from orders.models import Order, OrderStatus, OrderType


@register(Order)
class OrderTranslationOptions(TranslationOptions):
    fields = ('description',)


@register(OrderStatus)
class OrderStatusTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


@register(OrderType)
class OrderTypeTranslationOptions(TranslationOptions):
    fields = ('name', 'description')
