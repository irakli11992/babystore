from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.db import models
from django.forms import ModelForm
from django.forms.widgets import TextInput
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from items.models import Item, ItemAge, ItemBrand, ItemCategory, ItemColor
from reactfilebrowser.widgets import FileBrowserWidget


class ItemVariantAdmin(TranslationStackedInline):
    model = Item
    fk_name = "variant_of"
    extra = 1


class ItemForm(ModelForm):
    class Meta:
        model = Item
        widgets = {
            "images": FileBrowserWidget()
        }
        fields = "__all__"


@admin.register(Item)
class ItemAdmin(TranslationAdmin):
    form = ItemForm
    formfield_overrides = {
        models.TextField: {"widget": CKEditorWidget()},
    }
    fieldsets = (
        (None, {
            "fields": ("name", "sku", "is_active", "is_bestseller", "type", "description",)
        }),
        (
            "Properties",
            {
                "fields": (
                    'categories',
                    ('brand', 'age', 'color',),
                    ('variant_of', 'parent',),
                    ('specification_en', 'key_features_en'),
                    ('specification_ka', 'key_features_ka'),
                    "images",
                )
            }
        ),
        (
            "Meta",
            {
                "fields": ("slug", "meta_keywords", "meta_description")
            }
        ),
        ('Additional', {
            'classes': ('extrapretty', 'wide'),
            'fields': (
                ("price", "old_price"),
                ("updated_at", "created_at"),
            ),
        }),
    )
    filter_horizontal = ('categories',)
    list_filter = (
        ('age', admin.RelatedOnlyFieldListFilter),
        ("categories", admin.RelatedOnlyFieldListFilter),
        ("color", admin.RelatedOnlyFieldListFilter),
        ("brand", admin.RelatedOnlyFieldListFilter),
        'is_active',
        # 'is_variant',
    )
    search_fields = ('name', 'slug', 'description', "meta_keywords")
    readonly_fields = ("created_at", "updated_at")
    prepopulated_fields = {"slug": ("name",)}
    list_display = ("name", 'slug', "sku", "type", "brand", "age", "updated_at", "is_active", "in_stock", "is_variant")
    raw_id_fields = ("parent", "variant_of", "color")
    date_hierarchy = "created_at"
    # inlines = (ItemVariantAdmin,)
    list_per_page = 20

    class Media:
        js = [
            'js/collapsible_filters.js',
            'jquery/dist/jquery.min.js'
        ]


@admin.register(ItemAge)
class ItemAgeAdmin(admin.ModelAdmin):
    list_display = ('age_from', 'age_to', 'unit')


class ColorForm(ModelForm):
    class Meta:
        model = ItemColor
        fields = "__all__"
        widgets = {
            'value': TextInput(attrs={'cols': 8, 'rows': 2, "type": "color"}),
        }


@admin.register(ItemColor)
class ItemColorAdmin(TranslationAdmin):
    list_display = ('name', 'display_color')
    form = ColorForm


@admin.register(ItemBrand)
class ItemBrandAdmin(TranslationAdmin):
    pass


@admin.register(ItemCategory)
class ItemCategoryAdmin(TranslationAdmin):
    pass
