from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from items.models import Item, ItemColor, ItemAge


class ItemColorSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return str(obj)

    class Meta:
        model = ItemColor
        fields = ("id", "name", "value")


class ItemAgeSerializer(serializers.ModelSerializer):
    unit = serializers.SerializerMethodField()

    def get_unit(self, obj):
        return obj.get_unit_display()

    class Meta:
        model = ItemAge
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
    color = ItemColorSerializer(read_only=True)
    age = ItemAgeSerializer(read_only=True)
    type = serializers.SerializerMethodField()
    in_stock = serializers.SerializerMethodField()
    is_new = serializers.SerializerMethodField()

    class Meta:
        model = Item
        fields = '__all__'

    @staticmethod
    def get_type(obj):
        return obj.type, obj.get_type_display()

    @staticmethod
    def get_in_stock(obj):
        return obj.in_stock

    @staticmethod
    def get_is_new(obj):
        return obj.is_new


class ItemVariantSerializer(ItemSerializer):
    class Meta:
        model = Item
        fields = ('id', 'age', 'color', "in_stock", "sku", "price", "old_price", "name", "images")


class ItemDetailViewSerializer(ItemSerializer):
    variants = serializers.SerializerMethodField()
    translation = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    url = serializers.CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Item
        fields = (
            'translation',
            'id', 'age', 'color',
            'in_stock', 'sku', 'variants',
            'price', 'old_price', 'name',
            'description', 'images', 'rating', "url"
        )

    @staticmethod
    def get_variants(obj):
        serializer = ItemVariantSerializer(Item.item_manager.get_variants_of(obj.id), many=True)
        return serializer.data

    @staticmethod
    def get_translation(obj):
        return {
            "item_size": _("Age range"),
            "inStock": _("InStock"),
            "notInStock": _("Not Available"),
            "productId": _("Product Id"),
            "colors": _("Colors"),
            "go_to_details": _("Go to details"),
            "add_to_cart": _("Add to cart"),
            "review": _("Review"),
        }

    @staticmethod
    def get_rating(obj):
        rating = Item.item_manager.get_avg_rating(obj.id)
        return {'rating': rating[0], 'count': rating[1]}
