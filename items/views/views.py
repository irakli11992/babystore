from django.views.generic import ListView
from django.views.generic.detail import DetailView

from items.models import Item


class ProductList(ListView):
    model = Item
    template_name = "items/product_list/index.html"
    paginate_by = 2

    def get_queryset(self):
        return Item.item_manager.get_item_list()


class ProductDetails(DetailView):
    model = Item
    template_name = "items/single_product/index.html"

    def get_queryset(self):
        return Item.item_manager.select_related("brand")
