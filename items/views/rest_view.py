from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from items.serializers import ItemSerializer, ItemDetailViewSerializer
from items.models import Item


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 2


class ProductListApi(generics.ListAPIView):
    queryset = Item.item_manager.get_item_list()
    pagination_class = LargeResultsSetPagination
    serializer_class = ItemSerializer


class ProductDetailApi(generics.RetrieveUpdateDestroyAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemDetailViewSerializer
    lookup_field = 'slug'

    def get(self, *args, **kwargs):
        response = super().get(*args, **kwargs)
        response["Access-Control-Allow-Origin"] = "*"
        return response
