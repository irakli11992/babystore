from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from items.views.views import ProductList, ProductDetails
from items.views.rest_view import ProductListApi, ProductDetailApi

urlpatterns = [
    url(r'list/page/(?P<page>[0-9]+)/$', ProductList.as_view(), name="product_list"),
    url(r'listapi/page/(?P<page>[0-9]+)/$', ProductListApi.as_view(), name="product_list_api"),
    url(r'details/(?P<slug>[a-z0-9-]+)/$', ProductDetails.as_view(), name="product_details"),
    url(r'detailsapi/(?P<slug>[a-z0-9-]+)/$', ProductDetailApi.as_view(), name="product_details_api"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
