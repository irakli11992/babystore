from django import template
from django.conf import settings

from items.models import Item

register = template.Library()


@register.inclusion_tag("items/components/rating.html")
def rating_tag(item):
    return locals()


# @register.inclusion_tag("items/components/size.html")
# def size_tag(item: "Item"):
#     variants = Item.item_manager.get_sizes_of(
#         item.id
#     ).order_by(
#         "-age__unit", "age__age_from"
#     )
#     return locals()
#
#
# @register.inclusion_tag("items/components/color.html")
# def color_tag(item: "Item"):
#     variants = Item.item_manager.get_colors_of(item.id, item.age)
#     return locals()


@register.inclusion_tag("items/inclusion_tags/top_rated.html")
def top_rated():
    items = Item.item_manager.get_top_rated()[:12]
    MEDIA_URL = settings.MEDIA_URL

    return locals()
