from django.conf import settings

from items.models import ItemCategory, ItemAge, ItemBrand, ItemColor

from django import template

register = template.Library()


@register.inclusion_tag("items/filters/categories.html")
def categories_tag():
    categories = ItemCategory.objects.all()
    return locals()


@register.inclusion_tag("items/filters/ages.html")
def ages_tag():
    ages = ItemAge.objects.all()
    return locals()


@register.inclusion_tag("items/filters/price.html")
def price_tag():
    return {}


@register.inclusion_tag("items/filters/brands.html")
def brands_tag():
    brands = ItemBrand.objects.all()
    return locals()


@register.inclusion_tag("items/filters/discounts.html")
def discounts_tag():
    return {}


@register.inclusion_tag("items/filters/colors.html")
def colors_tag():
    colors = ItemColor.objects.all()
    return locals()


@register.filter()
def get_img_url(value, index):
    try:
        return "{0}{1}".format(settings.MEDIA_URL, value[index])
    except:
        return "#"
