from django.db import models, connection
from playhouse.shortcuts import cast


class ItemManager(models.Manager):
    def get_top_rated(self):
        from playhouse.djpeewee import translate
        from peewee import fn, JOIN, SQL

        from custom_comments.models import CommentWithTitle
        Item = self.model

        # translate django models to use in peewee
        custom_comments = translate(CommentWithTitle).CommentWithTitle
        items = translate(Item).Item

        # write subquery to get avg rating of items
        subquery = items.select(
            items.slug,
            (
                fn.SUM(custom_comments.rating) / fn.COUNT(custom_comments.id)
            ).alias(
                'rating'
            )
        ).join(
            custom_comments,
            JOIN.LEFT_OUTER,
            on=(cast(items.id, 'VARCHAR(1000)') == custom_comments.object_pk)
        ).where(
            items.variant_of >> SQL('NULL'),
        ).group_by(items.slug).alias('r')

        # select items with avg rating ordered by rating desc
        sql_text, param = items.select(
            items,
            subquery.c.rating
        ).join(
            subquery,
            on=(items.slug == subquery.c.slug)
        ).order_by(items.slug.desc(), subquery.c.rating.desc()).sql()
        return Item.objects.raw(sql_text, param)

    def get_item_list(self):
        from playhouse.djpeewee import translate
        from peewee import JOIN, SQL
        from items.models import Item, ItemAge
        from stock.models import StockItemMembership, Stock
        from playhouse.shortcuts import case

        trans = translate(Item, Stock, StockItemMembership, ItemAge)
        items = trans.Item  # type: Model
        ages = trans.ItemAge  # type: Model
        membership = trans.StockItemMembership  # type: Model

        sql_str, params = items.select(
            items,
            ages,
            case(
                None,
                (
                    (membership.quantity >> SQL('NULL'), SQL('FALSE')),
                    (membership.quantity > 0, SQL('TRUE'))
                ),
                SQL('FALSE')
            ).alias('_in_stock')
        ).join(
            ages,
            JOIN.LEFT_OUTER,
            on=(items.age == ages.id)
        ).join(
            membership,
            JOIN.LEFT_OUTER,
            on=(items.id == membership.item)
        ).where(
            items.is_active,
            items.variant_of >> SQL('NULL')
        ).order_by(
            items.created_at.desc(), items.updated_at.desc()
        ).sql()
        item_list = Item.objects.raw(sql_str, params)

        def _count():
            return lambda: len(list(item_list))

        item_list.count = _count()
        return item_list

    def get_item_list_1(self):
        cursor = connection.cursor()
        cursor.execute("""SELECT
              "items_item"."id",
              "items_item"."name",
              "items_item"."name_ka",
              "items_item"."name_en",
              "items_item"."slug",
              "items_item"."unique_code",
              "items_item"."sku",
              "items_item"."is_active",
              "items_item"."is_bestseller",
              "items_item"."type",
              "items_item"."description",
              "items_item"."description_ka",
              "items_item"."description_en",
              "items_item"."brand_id",
              "items_item"."age_id",
              "items_item"."meta_keywords",
              "items_item"."meta_description",
              "items_item"."price",
              "items_item"."old_price",
              "items_item"."created_at",
              "items_item"."updated_at",
              "items_item"."parent_id",
              "items_item"."variant_of_id",
              "items_itemage"."id",
              "items_itemage"."age_from",
              "items_itemage"."age_to",
              "items_itemage"."unit",
              "items_itemage"."description",
              CASE
              WHEN "stock_stockitemmembership"."quantity" IS NULL
                THEN FALSE
              WHEN "stock_stockitemmembership"."quantity" > 0
                THEN TRUE
              ELSE FALSE
              END AS "in_stock"

            FROM "items_item"
              LEFT OUTER JOIN "items_itemage" ON ("items_item"."age_id" = "items_itemage"."id")
              LEFT OUTER JOIN stock_stockitemmembership ON ("items_item"."id" = "stock_stockitemmembership"."item_id")
            WHERE ("items_item"."is_active" = TRUE AND "items_item"."variant_of_id" IS NULL)
            ORDER BY "items_item"."created_at" DESC, "items_item"."updated_at" DESC""")

        result_list = []
        for row in cursor.fetchall():
            p = self.model(
                id=row[0], name=row[1], name_ka=row[2], name_en=row[3],
                slug=row[4], unique_code=row[5], sku=row[6],
                is_active=row[7], is_bestseller=row[8], type=row[9],
                description=row[10], description_ka=row[11], description_en=row[12],
                brand_id=row[13], meta_keywords=row[15],
                meta_description=row[16], price=row[17], old_price=row[18],
                created_at=row[19], updated_at=row[20], parent_id=row[21],
                variant_of_id=row[22]
            )
            p.age = p._meta.get_field("age").rel.to(
                id=row[23], age_from=row[24], age_to=row[25],
                unit=row[26], description=row[27]
            )
            p._in_stock = row[28]
            result_list.append(p)
        return result_list

    def get_variants_of(self, variant_of_id):
        queryset = super().get_queryset().filter(
            is_active=True,
            variant_of=variant_of_id,
        )
        return queryset

    def get_colors_of(self, variant_of_id, age):
        return self.get_variants_of(
            variant_of_id
        ).filter(
            age=age
        ).select_related(
            "color"
        ).only(
            "color__name",
            "color__value",
            "age_id"
        )

    def get_sizes_of(self, variant_of_id):
        return self.get_variants_of(
            variant_of_id
        ).select_related(
            "age"
        ).only(
            "age__age_from",
            "age__age_to",
            "age__unit",
            "color__name",
            "color__value"
        )

    def get_avg_rating(self, id):
        from playhouse.djpeewee import translate
        from peewee import fn
        from custom_comments.models import CommentWithTitle
        from django.db import connection

        peewee_obj = translate(CommentWithTitle)
        sql_str, param = peewee_obj.CommentWithTitle.select(
            (
                fn.SUM(peewee_obj.CommentWithTitle.rating) / fn.COUNT(peewee_obj.CommentWithTitle.id)
            ).alias("rating")
            , fn.COUNT(peewee_obj.CommentWithTitle.id).alias("count")
        ).where(
            peewee_obj.CommentWithTitle.object_pk == str(id)
        ).sql()

        cursor = connection.cursor()
        cursor.execute(sql_str, param)
        return cursor.fetchone()
