from modeltranslation.translator import register, TranslationOptions
from items.models import Item, ItemBrand, ItemCategory, ItemColor


@register(Item)
class ItemTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


@register(ItemBrand)
class ItemBrandTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'country')


@register(ItemColor)
class ItemColorTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ItemCategory)
class ItemCategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'description')
