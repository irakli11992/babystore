import uuid
from enum import Enum

from django.contrib.postgres.fields.array import ArrayField
from django.utils.datetime_safe import datetime
from django.utils.timezone import utc
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language

from items.managers import ItemManager


class ItemTypeEnum(Enum):
    MAIN_ITEM = 0
    SUB_ITEM = 1

    @staticmethod
    def get_choices():
        return (
            (ItemTypeEnum.MAIN_ITEM.value, _("Main item")),
            (ItemTypeEnum.SUB_ITEM.value, _("Sub item"))
        )


class Item(models.Model):
    item_manager = ItemManager()
    objects = models.Manager()
    name = models.CharField(max_length=150, verbose_name=_('Name'),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 150})
    slug = models.SlugField(max_length=50, unique=True,
                            help_text=_('Unique value for product page URL, created from name.'))
    unique_code = models.UUIDField(default=uuid.uuid4)
    sku = models.CharField(max_length=50, help_text=_("Stock-keeping unit"), unique=True)
    #
    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))
    is_bestseller = models.BooleanField(default=False)
    key_features_en = ArrayField(models.CharField(max_length=150), verbose_name=_("Key Features"), blank=True,
                                 null=True,
                                 size=10)
    specification_en = ArrayField(models.CharField(max_length=150), verbose_name=_("Specification"), blank=True,
                                  null=True,
                                  size=10)
    key_features_ka = ArrayField(models.CharField(max_length=150), verbose_name=_("Key Features") + "[ka]", blank=True,
                                 null=True,
                                 size=10)

    specification_ka = ArrayField(models.CharField(max_length=150), verbose_name=_("Specification") + "[ka]",
                                  blank=True,
                                  null=True,
                                  size=10)
    type = models.IntegerField(choices=ItemTypeEnum.get_choices(), default=ItemTypeEnum.MAIN_ITEM.value,
                               verbose_name=_("Type"))
    description = models.TextField(verbose_name=_('Description'))

    brand = models.ForeignKey('ItemBrand', verbose_name=_('Brand'), null=True, blank=True)
    age = models.ForeignKey('ItemAge', verbose_name=_("Age range"))
    color = models.ForeignKey('ItemColor', verbose_name=_("Color"))
    categories = models.ManyToManyField("ItemCategory", verbose_name=_("Categories"), blank=True)
    images = ArrayField(models.CharField(max_length=9999), verbose_name=_("Images"), blank=True, null=True, size=20)
    meta_keywords = models.CharField(max_length=255,
                                     help_text=_('Comma-delimited set of SEO keywords for meta tag'))
    meta_description = models.CharField(max_length=255,
                                        help_text=_('Content for description meta tag'))
    price = models.DecimalField(default=0, decimal_places=2, max_digits=9, verbose_name=_("Price"))
    old_price = models.DecimalField(default=0, decimal_places=2, max_digits=9, verbose_name=_("Old price"))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Created date"))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_("Update time"))

    parent = models.ForeignKey("self", null=True, blank=True, verbose_name=_("Parent"))
    variant_of = models.ForeignKey("self", null=True, blank=True, verbose_name=_("Variant of"),
                                   related_name="variant_fo")

    def __str__(self):
        return self.name_ka if get_language() == "ka"else self.name_en

    @property
    def is_variant(self):
        return self.variant_of is not None

    @property
    def in_stock(self):
        if hasattr(self, "_in_stock"):
            return self._in_stock
        return self.stockitemmembership_set.filter(quantity__gt=0).count() > 0

    @property
    def is_new(self):
        now = datetime.utcnow().replace(tzinfo=utc)
        time_diff = now - self.created_at
        return time_diff.total_seconds() < 60 * 60 * 24 * 3

    # in_stock.short_description = _("In stock")
    @models.permalink
    def get_absolute_url(self):
        # from django.core.urlresolvers import reverse
        return "items:product_details", (), {"slug": self.slug}

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')
        ordering = ["-created_at", "-updated_at"]


class ItemColor(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Name"),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 200})
    value = models.CharField(max_length=10, verbose_name=_("Name"),
                             help_text=_('Maximum %(count)d symbols are allowed') % {"count": 10})

    @property
    def display_color(self):
        return mark_safe(
            "<div><div style='display:inline-block;background-color:{0}; width:3rem;height:1rem;'></div> <span>{1}</span></div>".format(
                self.value,
                self.value
            ))

    def __str__(self):
        return self.name_ka if get_language() == "ka" else self.name_en

    class Meta:
        verbose_name = _('Color')
        verbose_name_plural = _('Colors')


class ItemBrand(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Name"),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 200})
    description = models.TextField(verbose_name=_("Description"))

    country = models.CharField(max_length=200, verbose_name=_("Country"),
                               help_text=_('Maximum %(count)d symbols are allowed') % {"count": 200})

    def __str__(self):
        return self.name_ka if get_language() == "ka" else self.name_en

    class Meta:
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')


class AgeUnitEnum(Enum):
    YEAR = 0
    MONTH = 1

    @staticmethod
    def get_choices():
        return (
            (AgeUnitEnum.YEAR.value, _("Year")),
            (AgeUnitEnum.MONTH.value, _("Month"))
        )


class ItemAge(models.Model):
    age_from = models.IntegerField(verbose_name=_("From age"))
    age_to = models.IntegerField(verbose_name=_("To age"))
    unit = models.IntegerField(choices=AgeUnitEnum.get_choices(), verbose_name=_("unit"))
    description = models.TextField(verbose_name=_("Description"))

    def __str__(self):
        return "{0}-{1} {2}".format(self.age_from, self.age_to, self.get_unit_display())

    class Meta:
        verbose_name = _('Age range')
        verbose_name_plural = _('Age ranges')


class ItemCategory(models.Model):
    unique_code = models.UUIDField(
        verbose_name=_('Unique code'),
        default=uuid.uuid4,
        editable=False
    )
    name = models.CharField(max_length=150, verbose_name=_('Name'),
                            help_text=_('Maximum %(count)d symbols are allowed') % {"count": 150})
    description = models.TextField(verbose_name=_('Description'))

    # parent = models.ForeignKey("self", verbose_name=_("Parent"), null=True, blank=True)

    def __str__(self):
        return self.name_ka if get_language() == "ka"else self.name_en

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
