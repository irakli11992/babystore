"""babystore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import ugettext_lazy as _
from items import urls as items
from reactfilebrowser import urls as filebrowser
from django.conf.urls.static import static
from filebrowser.sites import site

urlpatterns = [
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^reactfilebrowser/', include(filebrowser, namespace="reactfilebrowser")),
    url(r'^items/', include(filebrowser, namespace="reactfilebrowser")),
    url(r'^comments/', include('django_comments.urls')),
    # url(r'^custom_comments/', include('custom_comments.urls', namespace="custom_comments")),
]

urlpatterns += i18n_patterns(
    url(_(r'^admin/'), admin.site.urls),
    url(_(r'^items/'), include(items, namespace="items")),
    url(_(r'^ckeditor/'), include('ckeditor_uploader.urls')),
    url(_(r'^custom_comments/'), include('custom_comments.urls', namespace="custom_comments")),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    import debug_toolbar

    urlpatterns += [
        # url(
        #     r'^media/(?P<path>.*)$', serve,
        #     {
        #         'document_root': settings.MEDIA_ROOT
        #     }
        # ),
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
